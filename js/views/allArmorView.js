var ArmorApp = ArmorApp || {};

ArmorApp.allArmorView = Backbone.View.extend({
	tagName: "section",
	render: function() {
		this.collection.each(this.addArmor, this);
		return this;
	},
	addArmor: function(armor){
		var armorView = new ArmorApp.singleArmorView({model: armor});
		this.$el.append(armorView.render().el);
	}
});

