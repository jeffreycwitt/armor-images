var ArmorApp = ArmorApp || {};

ArmorApp.singleArmorView = Backbone.View.extend({
	tagName: "article",
	className: "armorListItem",
	template: _.template( $("#armorElement").html()),
	render: function(){
		var armorTemplate = this.template(this.model.toJSON());
		this.$el.html(armorTemplate);
		return this;
	},
	events: {
		'mouseover': 'addBgColor',
		'mouseout': 'removeBgColor'
	},
	addBgColor: function() {
		//this.$el.addClass("bgColorImage");
		this.$el.find(".captionWrapper").show();
	},
	removeBgColor: function(){
		//this.$el.removeClass("bgColorImage");
		this.$el.find(".captionWrapper").hide();
	}

});

