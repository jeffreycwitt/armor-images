//Namespace of our app
var ArmorApp = ArmorApp  || {};

ArmorApp.singleArmor = Backbone.Model.extend({ 

	defaults: {
		color: "black",
		//FlickrUrl: "https://farm4.staticflickr.com/3707/19965776362_bbca4f29e9_m.jpg",
		idAttribute: 'id'
	},
	initialize: function(){
		//console.log("A model instance named " + this.get("Filename"));
		
		
		 
		this.on("change", function(){
			console.log("something in our model has changed");
		
		});
			
		this.on("change:Filename", function(){
			console.log("The filename for image " + this.get("id") + " model just changed to " + this.get("Filename"));
		});
		
	}


});

