var ArmorApp = ArmorApp || {};

ArmorApp.Router = Backbone.Router.extend({

	routes: {
		"": "noCopy",
		"search/:category/:query": "showSearch",
		":id": "showImage"
	},
	noCopy: function(){

			$("#copy").hide("slow");
			$("#copy-body").html("");
		
	},
	showImage: function(id){
		$("#copy").show("slow", function(){
		//var armorGroup = new ArmorApp.ArmorCollection();
			//armorGroup.fetch().then(function(){
				var filename = armorGroup.get(id).attributes.FlickrUrlBig;
				$("#copy-body").html("<img src=" + filename + "/>");
			//});
		});
	},
	showSearch: function(category, query){
		var filteredArmorGroup = new ArmorApp.ArmorCollection();
		filteredArmorGroup.fetch().then(function(){
			console.log(category, query);
			if (category == "Country"){
				filter = armorGroup.where({Country: query});
			}
			else if (category == "Date"){
				filter = armorGroup.where({Date: query});	
			}
			else if (category == "Century"){
				filter = armorGroup.where({Century: query});	
			}
  		
  		console.log(filter);
  		filteredCollection = new ArmorApp.ArmorCollection(filter);
  		var armorGroupView = new ArmorApp.allArmorView({collection: filteredCollection});
  		$("#allArmor").html(armorGroupView.render().el);
		});
	}
	

});