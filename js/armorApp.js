
//create an array of models and then pass them in collection creation method
var armorGroup = new ArmorApp.ArmorCollection();

//console.log(armorGroup.fetch());
armorGroup.fetch().then(function(){
  //console.log(armorGroup.toJSON;
  var armorGroupView = new ArmorApp.allArmorView({collection: armorGroup});
  $("#allArmor").html(armorGroupView.render().el);
});

var armorRouter = new ArmorApp.Router();

Backbone.history.start();





//flickr api key: 172621a04b1fb6853316e928014e4ec9

//
function setFlickrUrl(model){
	item = model;
		var flickrapi = "https://api.flickr.com/services/rest/?&method=flickr.photos.getSizes&api_key=172621a04b1fb6853316e928014e4ec9&photo_id=" + item.get("Flickrid") + "&format=json&jsoncallback=?"; 
		sources = getFlickrSources(flickrapi);
		sources.then(function(data){
			sourceArray = parseFlickrResponse(data);
			FlickrSmall = sourceArray[0].FlickrSmall;
			//console.log (FlickrSmall);

			item.set("FlickrUrl", "test");
			//console.log(item);
			
		});

}

//returns a promise
var getFlickrSources = function(flickrapi){
  flickrResponse = $.ajax({
      url: flickrapi,
      // The name of the callback parameter, as specified by the YQL service
      jsonp: "callback",
      // Tell jQuery we're expecting JSONP
      dataType: "jsonp"})
    
  return flickrResponse;
}

var parseFlickrResponse = function(data){
  flickrSourceArray = []
  
  if (data.stat == "ok"){
    sizeArray = data.sizes.size;
    for (var y in sizeArray){
      if (sizeArray[y].label == "Small"){
        flickrSourceArray.push({"FlickrSmall": sizeArray[y].source});
      }
      else if (sizeArray[y].label == "Large"){
        flickrSourceArray.push({"FlickrLarge": sizeArray[y].source});
      }
    }
  }
  
  return flickrSourceArray
}