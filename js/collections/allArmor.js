var ArmorApp = ArmorApp || {};


ArmorApp.ArmorCollection = Backbone.Collection.extend({
	model: ArmorApp.singleArmor,
	url: "https://spreadsheets.google.com/feeds/list/1SjHIBLTFb1XrlrpHxZ4SLE9lEJf4NyDVnKnbVejlL4w/1/public/values?alt=json",
  //comparator: "Century",
  parse: function(data){
    var armorarray = [];
    var entryarray = data.feed.entry;
    for (var x in entryarray){
        armorarray.push({"id": entryarray[x].gsx$id.$t,
                          "Filename": entryarray[x].gsx$filename.$t, 
                          "Century": entryarray[x].gsx$century.$t,
                          "Date": entryarray[x].gsx$date.$t,
                          "Country": entryarray[x].gsx$country.$t,
                          "City": entryarray[x].gsx$city.$t,
                          "Type": entryarray[x].gsx$type.$t,
                          "Maker": entryarray[x].gsx$maker.$t,
                          "Recepient": entryarray[x].gsx$recipient.$t,
                          "Flickrid": entryarray[x].gsx$flickrid.$t,
                          "FlickrUrl": entryarray[x].gsx$flickrurl.$t,
                          "FlickrUrlBig": entryarray[x].gsx$flickrurlbig.$t,
                        });
      }
      return armorarray;
  }
});
  
/*
  fetch: function(options){
    var coll = this;

    return $.getJSON(this.url).then(function(data) {
      var armorarray = [];
      var entryarray = data.feed.entry;
      
    for (var x in entryarray){
        armorarray.push({"id": entryarray[x].gsx$id.$t,
                          "Filename": entryarray[x].gsx$filename.$t, 
                          "Century": entryarray[x].gsx$century.$t,
                          "Date": entryarray[x].gsx$date.$t,
                          "Country": entryarray[x].gsx$country.$t,
                          "City": entryarray[x].gsx$city.$t,
                          "Type": entryarray[x].gsx$type.$t,
                          "Maker": entryarray[x].gsx$maker.$t,
                          "Recepient": entryarray[x].gsx$recipient.$t,
                          "Flickrid": entryarray[x].gsx$flickrid.$t,
                          "FlickrUrl": "", //entryarray[x].gsx$flickrurl.$t,
                          "FlickrUrlBig": ""//entryarray[x].gsx$flickrurlbig.$t,
                        });
      }

    var imagePromises = armorarray.map(function(item) {
        var flickrapi = "https://api.flickr.com/services/rest/?&method=flickr.photos.getSizes&api_key=172621a04b1fb6853316e928014e4ec9&photo_id=" + item.Flickrid + "&format=json&jsoncallback=?"; 
        //var flickrapi = "https://api.flickr.com/services/rest/?&method=flickr.photos.getSizes&api_key=172621a04b1fb6853316e928014e4ec9&photo_id=19965776362&format=json&jsoncallback=?";
        return getFlickrSources(flickrapi).then(function(flickrResponse) {
          item.FlickrUrl = parseFlickrResponse(flickrResponse)[0].FlickrSmall;
        });
      });

    return $.when(imagePromises).then(function() {
        //console.log(armorarray);

        coll.set(armorarray);
        //armorarray;
        
      });

    });

  }
});


//returns a promise
var getFlickrSources = function(flickrapi){
  flickrResponse = $.ajax({
      url: flickrapi,
      // The name of the callback parameter, as specified by the YQL service
      jsonp: "callback",
      // Tell jQuery we're expecting JSONP
      dataType: "jsonp"})
    
  return flickrResponse;
}

var parseFlickrResponse = function(data){
  flickrSourceArray = []
  
  if (data.stat == "ok"){
    sizeArray = data.sizes.size;
    for (var y in sizeArray){
      if (sizeArray[y].label == "Small"){
        flickrSourceArray.push({"FlickrSmall": sizeArray[y].source});
      }
      else if (sizeArray[y].label == "Large"){
        flickrSourceArray.push({"FlickrLarge": sizeArray[y].source});
      }
    }
  }
  
  return flickrSourceArray
}
*/